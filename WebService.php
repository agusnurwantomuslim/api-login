<?php 

include 'koneksi.php';
include 'fungsi.php';

$z=ud(@$_REQUEST['z']);
#$z=ud('bmF2PWhhcmdhJnByb3BfaWQ9NCZ6b25hPUNeZGVsaW1pdGVy');

#Untuk Mendapatkan daftar propinsi
if(@$z['nav']=='prop'){
	$query = mysqli_query($connect,"select * from propinsi");
	while( $data = mysqli_fetch_array( $query ) ){
		$dt['prop_nama'][]=$data['prop_nama'];
		$dt['prop_id'][]=$data['prop_id'];
	}
}

#Untuk Mendapatkan daftar Kabupaten
if($z['nav']=='kab'){
	$prop_id = mysqli_real_escape_string( $connect,$z['prop_id'] );
	$query = mysqli_query($connect,"select * from kabupaten where prop_id=$prop_id");
	while( $data = mysqli_fetch_array( $query ) ){
		$dt['kab_nama'][]=$data['kab_nama'];
		$dt['kab_id'][]=$data['kab_id'];
	}
}

#Untuk Mendapatkan daftar Kecamatan
if($z['nav']=='kec'){
	$kab_id = mysqli_real_escape_string( $connect,$z['kab_id'] );
	$query = mysqli_query($connect,"select * from kecamatan where kab_id=$kab_id");
	while( $data = mysqli_fetch_array( $query ) ){
		$dt['kec_nama'][]=$data['kec_nama'];
		$dt['prop_id'][]=$data['prop_id'];
		$dt['zona'][]=$data['zona'];
	}
	$dt['q'] = "select * from kecamatan where kab_id=$kab_id";
}

#Untuk Menampilkan harga
if($z['nav']=='harga'){
	$query2 = mysqli_query($connect,"select meta_value from wp_config where meta_key='id_toko' AND meta_value='1'");
	while( $data2 = mysqli_fetch_array( $query2 ) ){
		$z['id_toko'][] = $data2['meta_value'];
	}
	$prop_id = mysqli_real_escape_string( $connect,$z['prop_id'] );
	$zona = mysqli_real_escape_string( $connect,$z['zona'] );
	$id_toko = mysqli_real_escape_string( $connect,$z['id_toko'][0] );
	$query = mysqli_query($connect,"select harga from harga where zona='$zona' AND prop_id=$prop_id AND id_toko=$id_toko");
	while( $data = mysqli_fetch_array( $query ) ){
		$dt['harga'][]='Tarif: Rp '.$data['harga'].',00';
	}
}

#validasi jika input salah
if( ($z['nav']=='prop') || ($z['nav']=='kab') || ($z['nav']=='kec') || ($z['nav']=='harga') ){
	$dt['ket'][]='success!';
}else{
	$dt['ket'][]='Data kabupaten/kecamatan tidak ditemukan, cek kembali data yang dimasukan.';
}

header('content-type : aplication/json');
	echo json_encode(
			$dt
		);

?>
