-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 29, 2013 at 01:15 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shiping`
--

-- --------------------------------------------------------

--
-- Table structure for table `harga`
--

CREATE TABLE IF NOT EXISTS `harga` (
  `id_harga` int(11) NOT NULL AUTO_INCREMENT,
  `prop_id` int(11) NOT NULL,
  `zona` enum('A','B','C','D') NOT NULL,
  `harga` text NOT NULL,
  `id_toko` int(11) NOT NULL,
  PRIMARY KEY (`id_harga`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `harga`
--

INSERT INTO `harga` (`id_harga`, `prop_id`, `zona`, `harga`, `id_toko`) VALUES
(1, 4, 'A', '5000', 1),
(2, 4, 'B', '7000', 1),
(3, 4, 'C', '8000', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kabupaten`
--

CREATE TABLE IF NOT EXISTS `kabupaten` (
  `kab_id` int(11) NOT NULL AUTO_INCREMENT,
  `kab_nama` text NOT NULL,
  `prop_id` int(11) NOT NULL,
  PRIMARY KEY (`kab_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `kabupaten`
--

INSERT INTO `kabupaten` (`kab_id`, `kab_nama`, `prop_id`) VALUES
(1, 'Sleman', 4),
(2, 'Bantul', 4);

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE IF NOT EXISTS `kecamatan` (
  `kec_id` int(11) NOT NULL AUTO_INCREMENT,
  `kec_nama` text NOT NULL,
  `kab_id` int(11) NOT NULL,
  `prop_id` int(11) NOT NULL,
  `zona` enum('A','B','C','D') NOT NULL,
  PRIMARY KEY (`kec_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kecamatan`
--

INSERT INTO `kecamatan` (`kec_id`, `kec_nama`, `kab_id`, `prop_id`, `zona`) VALUES
(1, 'Ngaglik', 1, 4, 'A'),
(2, 'kec 2', 1, 4, 'B'),
(3, 'Banguntapan', 2, 4, 'C');

-- --------------------------------------------------------

--
-- Table structure for table `propinsi`
--

CREATE TABLE IF NOT EXISTS `propinsi` (
  `prop_id` int(11) NOT NULL AUTO_INCREMENT,
  `prop_nama` text NOT NULL,
  PRIMARY KEY (`prop_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `propinsi`
--

INSERT INTO `propinsi` (`prop_id`, `prop_nama`) VALUES
(1, 'Jawa Timur'),
(2, 'Jawa Tengah'),
(3, 'Jawa Barat'),
(4, 'Yogykarta'),
(5, 'Banten');

-- --------------------------------------------------------

--
-- Table structure for table `wp_config`
--

CREATE TABLE IF NOT EXISTS `wp_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `meta_key` text NOT NULL,
  `meta_value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `wp_config`
--

INSERT INTO `wp_config` (`id`, `id_user`, `meta_key`, `meta_value`) VALUES
(1, 1, 'id_toko', '1'),
(2, 1, 'toko', 'Yogyakarta');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
